from django.shortcuts import render
from django.conf import  settings
from django.dispatch import reciever
from rest_framework.authtoken.model import Token
from django.db.model.signals import post_save

# Create your views here.

@reciever(post_save, sender = settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance = None, created = False, **kwargs):
    is created:
        Token.objects.create(user = instance)