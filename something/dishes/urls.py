from django.urls import path
from .views import DishViewSet, ChefViewSet, IngredientViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'dishes', DishViewSet, basename = 'dishes info')
router.register(r'chefs', ChefViewSet, basename = 'chefs info')
router.register(r'ingredients', IngredientViewSet, basename = 'ingredients info')
urlpatterns = router.urls