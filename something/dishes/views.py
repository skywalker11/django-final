from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework import viewsets
from django.shortcuts import get_object_or_404
 
# Create your views here.



    
class ChefViewSet(viewsets.ModelViewSet):
    serializer_class = ChefSerializer
    queryset = Chef.objects.all()
    
class DishViewSet(viewsets.ModelViewSet):
    serializer_class = DishSerializer
    queryset = Dish.objects.all()
    
class IngredientViewSet(viewsets.ModelViewSet):
    serializer_class = IngredientSerializer
    queryset = Ingredient.objects.all()
